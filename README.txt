Variables list and description - Slider Voting Module :


  //To add new widget 
      $templates = array();
      $templates['slider'] = new stdClass();
      $templates['slider']->value_type = 'percent';
      $templates['slider']->options = _slider_widget_options();
      $templates['slider']->theme = 'rate_template_slider';
      $templates['slider']->css = drupal_get_path('module', 'slider') . '/templates/slider/slider.css';
      $templates['slider']->js = drupal_get_path('module', 'slider') . '/templates/slider/slider.js';
      $templates['slider']->customizable = FALSE;
      $templates['slider']->translate = FALSE;
      $templates['slider']->template_title = t('Slider');


  //check user vote from "votingapi_vote" table
      $sql = "select vote_id from votingapi_vote where entity_type = :type and entity_id = :nid and
      uid =:uid and vote_source = :host";
      $data = db_query($sql, array(
      ':type' => $var['content_type'],
      ':nid' => $var['content_id'],
      ':uid' => $var['user']->uid,
      ':host' => $var['user']->hostname
      )
      )->fetch();


  //get slider minimum value from config
      $min = variable_get("slider_min",0);


  //get slider maximum value from config
      $max = variable_get("slider_max",10);


  //calculate the average between maximum to minimum
      $avg = ($max - $min) / 2;


  //for slider possition
      $pos = intval($variables['results']['rating'])/100 * ($max - $min);


  //if logged in user and has voted already, then let our config decide the widget display
      $show_slider = variable_get('slider_delete_vote', true);
      $pos_user = intval($variables['results']['user_vote'])/100 * ($max - $min);


  //if anon user and config is not allowing vote alteration, then check if he has already voted based on hostname.
      $show_slider = !slider_votingapi_has_voted($variables);    


  //for rendering slider button
      $button = theme('rate_button', array('text' => $link['text'], 'href' => $link['href'], 'class' => ''));
      $buttons[] = $button;


  //get current user votes
      $user_vote = number_format($pos_user + $min, 0);


  //render css class to the slider widget
      $variables['css_class'] = variable_get('slider_css_class','');


  //render css Id to the slider widget
      $variables['css_id'] = variable_get('slider_css_id','');


  //render data attribute to the slider widget
      $variables['data_attr'] = _slider_get_data_attr($variables, $user_vote);


  //set the default value of maximum - minimum
      $slider_value = 5;


  //get the dynamic value of maximum - minimum
      $slider_value = $form_state['values']['max'] - $form_state['values']['min'];


  //Slider Rate Widget General Settings Form
      $form['#prefix'] = '<div id="slider-updateform">';
      $form['#suffix'] = '</div>';


  //General Settings fieldset
      $form['general'] = array(
        '#type' => 'fieldset',
        '#title' => t('General Settings'),
      '#description' => t('Slider Rate Widget General Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );


  //Checkbox for user's to delete/change their votes 
      $form['general']['delete_vote'] = array(
       '#type' => 'checkbox',
       '#title' => t("Enable user's to delete/change their votes"),
       '#default_value' => variable_get('slider_delete_vote',true)
       //'#description' => t(""),
      );


  //Choose your Style vertical/horizontal
      $form['general']['style'] = array(
       '#type' => 'select',
       '#title' => t("Choose your Style"),
       '#options' => array(
          'vertical' => t('Vertical'),
          'horizontal' => t('Horizontal'),
        ),
      '#default_value' => variable_get('slider_style','horizontal')
      );


  //Show Submit Button
      $form['general']['vote_button'] = array(
       '#type' => 'checkbox',
       '#title' => t("Show Submit Button"),
       '#default_value' => variable_get('slider_submit_button', 0)
      );


  //To add button label
      $form['general']['label'] = array(
        '#type' => 'textfield',
        '#title' => t('Button Label'),
        '#description' => t('Enter a label for the button. Default value : <b>Vote</b>'),
        '#default_value' => variable_get('slider_submit_label',t("Vote")),
        '#states' => array(
          // Show the settings when "Show Submit Button" checkbox is enabled.
          'visible' => array(
           ':input[name="vote_button"]' => array('checked' => TRUE),
          ),
        ),
      );


  //To add css Class for slider widget
      $form['general']['css_class'] = array(
        '#type' => 'textfield',
        '#title' => t("Css Class"),
        '#description' => t("Css Class for the slider widget"),
        '#default_value' => variable_get('slider_css_class','')
      );


  //To add css ID for slider widget
      $form['general']['css_id'] = array(
        '#type' => 'textfield',
        '#title' => t("Css Id"),
        '#description' => t("Css Id for the slider widget"),
        '#default_value' => variable_get('slider_css_id','')
      );  


  //Setting Min value for slider
      $form['value']['min'] = array(
        '#type' => 'select',
        '#title' => t('Minimum Value'),
        '#description' => t('Set minimum value'),
        '#options' => drupal_map_assoc(range(-10,10)),
        '#default_value' => variable_get('slider_min', -5),
        '#required' => TRUE,
        '#ajax' => array(
          'event' => 'change',
          'callback' => '_slider_values_update',
          'wrapper' => 'slider-updateform',
          'method' => 'replace'
        ),
      );


  //Setting Max value for slider
      $form['value']['max'] = array(
        '#type' => 'select',
        '#title' => t('Maximum Value'),
        '#description' => t('Set maximum value'),
        '#options' => drupal_map_assoc(range(-10,10)),
        '#default_value' => variable_get('slider_max', 5),
        '#required' => TRUE,
        '#ajax' => array(
          'event' => 'change',
          'callback' => '_slider_values_update',
          'wrapper' => 'slider-updateform',
          'method' => 'replace'
        ),
      );


  //Setting Increment value for slider
      $form['value']['increment'] = array(
        '#type' => 'select',
        '#title' => t('Slider increment Value'),
        '#description' => t('Set increment value'),
        '#options' => drupal_map_assoc(range(0.1,$slider_value,0.1)),
        '#default_value' => variable_get('slider_increment',1),
        '#required' => TRUE
      );


  //Setting Message for  slider  - On change and on focus
      $form['message'] = array(
        '#type' => 'fieldset',
        '#title' => t('Message'),
        '#description' => t('Slider Rate Widget - Message Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
  
  
  //onchange - Should be disabled if vote button is disabled
      $form['message']['onchange'] = array(
        '#type' => 'textfield',
        '#title' => t('On Change'),
        '#description' => t('Message to display when user slides the slider.!vote is the available token. Leave it blank for no message.'),
        '#default_value' => variable_get('slider_onchange_message',''),
        '#required' => FALSE,
        '#states' => array(
          // Show the settings when "Show Submit Button" checkbox is enabled.
          'visible' => array(
           ':input[name="vote_button"]' => array('checked' => TRUE),
          ),
        ),
      );
  
  
  //onsubmit - Should be disabled if vote button is disabled
      $form['message']['onsubmit'] = array(
        '#type' => 'textfield',
        '#title' => t('On Submit'),
        '#description' => t('Message to display when user submits a vote. !vote and !count are the available tokens. Leave it blank for no message.'),
        '#default_value' => variable_get('slider_submit_message',''),
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => FALSE,
      );
  
  
  //custom form validate
      $form['#validate'][] = 'slider_settings_form_validate';
  
 
  //custom form submit
      $form['#submit'][] = 'slider_widget_form_submit';


  //get current value of min
      $min = $form_state['values']['min'];


  //get current value of max
      $max = $form_state['values']['max'];


  //assign the difference of min to max
      $diff = $max - $min;


  // Set range values in widget_update
      $total = $form_values['max'] - $form_values['min'];
      $fraction = 100/$total;
      $values = array();
      for($i=0; $i<($total + 1); $i++) {
        $values[] = $i * $fraction;
      }
      $options = array_map( function($ele) { return array($ele, $ele); }, range($form_values['min'],$form_values['max'], 1) );
      $widget->options = $options;


  // Set range values widget_insert
      $total = $form_values['max'] - $form_values['min'];
      $fraction = 100/$total;
      $values = array();
      for($i=0; $i<($total + 1); $i++) {
        $values[] = $i * $fraction;
      }
      $options = array_map( function($ele) { return array($ele, $ele); }, range($form_values['min'],$form_values['max'], 1) );
      $widget->options = $options;


  //To convert votes value to %percentage
      $votes['value'] =  (100/ ($max - $min)) * $pos;
  
  //divide it with second value
    $res = $a/$b;
    if(_slider_has_fraction($res)) {
      return false;
    }
    return true;
    
  //get the fractional part for eg 50.123 - 50 => 0.123
    $has_fraction = abs(round($c) - $c) > 0.0001;    
