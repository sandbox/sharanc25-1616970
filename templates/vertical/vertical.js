var disabled = false;
(function ($) {
  Drupal.behaviors.RateSlider = {
    attach: function (context) {

      $('.rate-widget-slider:not(.rate-slider-processed)',context).addClass('rate-slider-processed').each(function() {
        var widget = $(this);
        var ids = widget.attr('id').match(/^rate\-([a-z]+)\-([0-9]+)\-([0-9]+)\-([0-9])$/);
        var data = {
          content_type: ids[1],
          content_id: ids[2],
          widget_id: ids[3],
          widget_mode: ids[4]
        };

        var s = $(".rate-slider", widget);
        var v = $(s).attr("class").match(/rate\-value\-([0-9]+)/)[1];
        var max =  Drupal.settings.slider.max;
        var min =  Drupal.settings.slider.max;
        var increment =  Drupal.settings.slider.max;
        
        v = (parseInt(v)*Drupal.settings.slider.increment) + parseInt(Drupal.settings.slider.min);
        widget.prepend(s);
        widget.bind("eventAfterRate", function(event, params) {
          $(".rate-info").text(Drupal.t(Drupal.settings.slider.submit_message, {"!vote": params.value}));
        });
        var total = parseInt(Drupal.settings.slider.max) - parseInt(Drupal.settings.slider.min);
        // Check if this widget is active (disabled widgets have <span>'s instead of <a>'s).'
        if ($("ul a", widget).length > 0) {
          // Add the slider.
          s.slider({
            min: parseInt(Drupal.settings.slider.min),
            max: parseInt(Drupal.settings.slider.max),
            steps: 90,
            startValue: v, // jQuery UI 1.2
            value: v, // jQuery UI 1.3
            step: parseFloat(Drupal.settings.slider.increment),
            orientation: "vertical",
            slide: function(event,ui) {
              //ui.value;

              var pos = sliderFindPos(data.value, Drupal.settings.slider.min);
              //var pos = (data.value - Drupal.settings.slider.min);
              var percentage =  sliderFindPerc(total, pos);//(100/total) * pos;
              var vote = ui.value;
              $(".rate-slider-value", s).width(percentage + '%');
              $(".rate-info", widget).text(Drupal.t(Drupal.settings.slider.onchange_message, {"!vote": vote.toFixed(1)}));
            },
            stop: function(event,ui) {
              data.value = ui.value;
              
              var pos = sliderFindPos(data.value, Drupal.settings.slider.min);
              var percentage =  sliderFindPerc(total, pos);
              var button_id  = pos + 1;//Math.abs(Math.round((percentage / ( Drupal.settings.slider.max -  Drupal.settings.slider.min)))) + 1;
              
              var itemid = "#rate-button-" + button_id;
              var token = $(itemid).attr('href').match(/rate\=([a-zA-Z0-9\-_]{32,64})/)[1];
              if(!Drupal.settings.slider.show_slider) {
                s.slider('disable');
                disabled = true;
              }
              //check if manual submit is required to fire vote
              if (!parseInt(Drupal.settings.slider.submit_button)) {
                return Drupal.rateVote(widget, data, token);
              }
              else {
                if($('#slider_submit_button', widget).length == 0) {
                  var button = "<div id='slider_submit_button'>";
                  button += "<input type='submit' value='" + Drupal.settings.slider.submit_label + "'/></div>";
                  $(widget).append(button);                  
                  $('#slider_submit_button', widget).live('click',function() {                            
                    return Drupal.rateVote(widget, data, token);
                  });
                }
              }
            }
          });
        }
        else {
          // Widget is disabled. Only add the slider styling.
          $(s).width('200px');
          $(s).addClass('ui-slider');
        }
        if(!Drupal.settings.slider.show_slider_user || disabled) {
          s.slider('disable');
        }

        // Add the rating bar.

        s.prepend('<div class="rate-slider-value" style="height: ' + (sliderFindPerc(total, $(s).attr("class").match(/rate\-value\-([0-9]+)/)[1])) + '%" />');
        // Hide the links for the non-js variant.
        $("ul", widget).hide();
      });
      /*$('#slider_submit_button').live('click',function() {
        console.log("Widget data");
        console.log(widget);
      });*/
    }

  }
})(jQuery);


function sliderFindPos(value, min) {
  var result = parseFloat(value - min)/parseFloat(Drupal.settings.slider.increment);
  return Math.round(result); 
}
function sliderFindPerc(total, pos) {
  return ((100/total) * pos) * parseFloat(Drupal.settings.slider.increment);
}
